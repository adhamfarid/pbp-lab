from django.shortcuts import render
from lab_2.models import Note
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url=" /admin/login/?next")
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)
