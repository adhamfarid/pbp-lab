from lab_1.models import Friend
from django import forms


class FriendForm(forms.ModelForm):

    # ini gaperluuuu, yang bawah aja yg perlu
    DOB = forms.CharField(widget=forms.TextInput(attrs={
        "class": "",
        "type": "date",
    }))

    class Meta:
        model = Friend
        fields = '__all__'

        # widgets = {
        #     'DOB': forms.DateField(DOB={'class': 'input'})
        # }
