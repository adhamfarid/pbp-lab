from django.shortcuts import render
from lab_1.models import Friend  # ambil metode dari models lab 1
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from .forms import FriendForm
# Create your views here.


@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


@login_required(login_url="/admin/login/")
def add_friend(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FriendForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})
