import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple[50],
      appBar: buildAppBar(),
      body: buildBody(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.black87.withOpacity(0.8),
      title: Text("Covid 19 Data",
          style: TextStyle(
            fontSize: 20,
            color: Colors.white,
          )),
    );
  }

  Container buildBody() {
    return Container(
        padding: const EdgeInsets.all(30),
        color: Colors.purple[100].withOpacity(0.4),
        child: Column(children: [
          Text("Data di Provinsi-mu",
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              )),
          SizedBox(height: 20),
          Container(
              padding: const EdgeInsets.only(
                left: 15,
                top: 17,
              ),
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.yellow[900], Colors.yellow[700]],
                  begin: Alignment.bottomLeft,
                  end: Alignment.centerRight,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Positif",
                        style: TextStyle(
                          color: Colors.white,
                        )),
                    Text("10,000",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                        )),
                    Text("Orang",
                        style: TextStyle(
                          color: Colors.white,
                        ))
                  ])),
          SizedBox(height: 20),
          Container(
              padding: const EdgeInsets.only(
                left: 15,
                top: 17,
              ),
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.green[900], Colors.green[700]],
                  begin: Alignment.bottomLeft,
                  end: Alignment.centerRight,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Sembuh",
                        style: TextStyle(
                          color: Colors.white,
                        )),
                    Text("10,000",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                        )),
                    Text("Orang",
                        style: TextStyle(
                          color: Colors.white,
                        ))
                  ])),
          SizedBox(height: 20),
          Container(
              padding: const EdgeInsets.only(
                left: 15,
                top: 17,
              ),
              width: MediaQuery.of(context).size.width,
              height: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.red[900], Colors.red[700]],
                  begin: Alignment.bottomLeft,
                  end: Alignment.centerRight,
                ),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Meninggal",
                        style: TextStyle(
                          color: Colors.white,
                        )),
                    Text("10,000",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                        )),
                    Text("Orang",
                        style: TextStyle(
                          color: Colors.white,
                        ))
                  ])),
          SizedBox(height: 20),
        ]));
  }
}
