Nomor 1) Apakah perbedaan antara JSON dan XML?

    JSON digunakan untuk merepresentasikan sebuah object, sementara XML digunakan untuk merepresentasikan data 
    
    JSON
    - Berbasis JavaScript language 
    - merepresentasikan objek
    - supports array
    - lebih mudah dibaca ketimbang XML
    - ga support comments
    - hanya support UTF-8
    
    XML
    - diambil dari SGML
    - represent data
    - ga support array
    - susah diimplement dalam bentuk code
    - lebih aman daripada JSON
    
    



Nomor 2) Apakah perbedaan antara HTML dan XML?:

    > HTML adalah sebuah bahasa yang digunakan untuk membuat website.
    > XML adalah programming language yang digunakan untuk men-define struktur dokumen untuk di-store dan di-transport

    


References:
[Nomor 1](https://www.geeksforgeeks.org/difference-between-json-and-xml/)
[Nomor 2](https://www.upgrad.com/blog/html-vs-xml/)

