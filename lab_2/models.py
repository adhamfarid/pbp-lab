from django.db import models

# Create your models here.


class Note(models.Model):
    to = models.CharField(max_length=30)
    from_whom = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.TextField(max_length=50)

    def __str__(self):
        return self.to
