from lab_2.models import Note
from django import forms


class NoteForm(forms.ModelForm):
    to = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'class': 'form', 'id': 'to'}))
    from_whom = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'class': 'form', 'id': 'from'}))
    title = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'class': 'form', 'id': 'title'}))
    message = forms.CharField(max_length=100, widget=forms.Textarea(
        attrs={'class': 'form', 'id': 'message'}))

    class Meta:
        model = Note
        fields = '__all__'
