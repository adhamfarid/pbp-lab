from .views import index, add_note, note_list
from django.urls import path

urlpatterns = [
    path('', index, name='index'),
    path('add', add_note, name='add'),
    path('note-list', note_list, name="note-list"),
]
